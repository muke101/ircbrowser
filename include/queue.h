#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>

#define BUFFSIZE 1000

struct queue	{
	char *buffer;
	int front;
	int back;
	sem_t sem;
};

void en_queue(struct queue *queue, char *key);

char *de_queue(struct queue *queue);

#endif

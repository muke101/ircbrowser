#include "main.h"

int soc;

void pong()	{
	char *indx;
	char response[IRC_MSG_LEN];  
	unsigned msg_size;

	if ((indx = strstr(msg, "PING:")))	{
		msg_size = IRC_MSG_LEN-(indx-response)*sizeof(char);
		sprintf(response, "PONG:%.*s\r\n", msg_size, indx+5);
		if (send(soc, response, msg_size, 0) == -1)	{
			printf("send failed, %s\n", strerror(errno));
			exit(1);
		}
	}
}

void reg(char *nick, char *channel)	{ 

	//NICK nick\r\n\0
	char nickMsg[5+strlen(nick)+3];
	sprintf(nickMsg, "NICK %s\r\n", nick);
	
	if (send(soc, nickMsg, strlen(nickMsg), 0) == -1)	{
		printf("send failed, %s\n", strerror(errno));			
		exit(1);
	}

	//USER username 0 * :username\r\n\0
	char userMsg[5+strlen(nick)+6+strlen(nick)+3];
	sprintf(userMsg, "USER %s 0 * :%s\r\n", nick, nick);

	if (send(soc, userMsg, strlen(userMsg), 0) == -1)	{
		printf("send failed, %s\n", strerror(errno));			
		exit(1);
	}

	pong(); //some servers require immediate pong response after nick registration

	//JOIN :#channel\r\n\0
	char chanMsg[7+strlen(channel)+3];
	sprintf(chanMsg, "JOIN :#%s\r\n", channel);

	if (send(soc, chanMsg, strlen(chanMsg), 0) == -1)	{
		printf("send failed, %s\n", strerror(errno));
		exit(1);
	}
}

void rec_msg(struct *thread_info T)	{
	int res;

	char *msg = malloc(IRC_MSG_LEN*sizeof(char));

	while ((res=recv(soc, msg, IRC_MSG_LEN, 0)))	{ 
		if (res < 0)	{
			printf("rec failed, %s\n", strerror(errno)); 
			exit(1);
		}
		pong(); //TODO: take out the ping request
		en_queue(msg);	
		msg = malloc(IRC_MSG_LEN*sizeof(char));
	}

	printf("connection closed\n");
	pthread_exit();
}

char *rec_page()	{

	char *indx, *page;

	page = malloc(PAGE_BUFF*sizeof(char));
	
}


int main(int argc, char **argv)	{

	char *url;

	url = *(argv+1);
	
	struct addrinfo *addr;

	size_t res;


	if (getaddrinfo(url, "irc", 0, &addr) == 0)	{
		printf("Lookup failed, %s\n",strerror(errno));
		return 1;
	}

	if ((soc = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol)) == -1)	{
		printf("socket creation failed, %s\n",strerror(errno));
		return 1;
	}

	if (connect(soc, addr->ai_addr, addr->ai_addrlen) == -1)	{
		printf("connection failed, %s\n", strerror(errno));
		return 1;
	}

	char nick[] = "mukebot", channel[] = "bots";
	reg(nick, channel);
	rec_page();

	return 0;
}

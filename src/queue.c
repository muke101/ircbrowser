#include "queue.h"

struct queue create_queue()	{
	size_t pagesize = getpagesize();
	size_t sz = ((BUFFSIZE*sizeof(char *))/pagesize)*(pagesize+1); //align to page
	int fd = fileno(tmpfile());

	void *buffer = mmap(NULL, 2*sz, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	mmap(buffer, sz, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED | MAP_FIXED, fd, 0);
	mmap(buffer+sz, sz, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED | MAP_FIXED, fd, 0);
	
	struct queue new_queue;

	new_queue.buffer = (char *)buffer;
	new_queue.front = 0;
	new_queue.back = 0;
	sem_init(&(new_queue.sem), 0, 0);

	return new_queue;
}

void en_queue(struct queue *queue, char *key)	{
	memcpy(queue->buffer+queue->back, &key, sizeof(char *));
	queue->back = (queue->back + sizeof(char *)) % BUFFSIZE;
	sem_post(&(queue->sem));
}

char *de_queue(struct queue *queue)	{
	char *key;
	sem_wait(&(queue->sem));
	key = queue->buffer+queue->front;
	queue->front = (queue->front + sizeof(char *)) % BUFFSIZE;
	return key;
}

